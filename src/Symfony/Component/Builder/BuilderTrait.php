<?php
/**
 * @file
 * BaseBuilder.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * BaseBuilder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait BuilderTrait
{
    use ContainerAwareTrait;

    /**
     *
     * @var BuilderInterface
     */
    protected $builder;

    /**
     * Default constructor.
     *
     * @param BuilderInterface $builder
     *            Builder.
     */
    public function __construct(BuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Return builder.
     */
    public function getBuilder()
    {
        return $this->builder;
    }
}