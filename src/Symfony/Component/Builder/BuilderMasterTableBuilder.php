<?php
/**
 * @file
 * Master table builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class BuilderMasterTableBuilder implements ContainerAwareInterface
{
    use BuilderTrait;

    /**
     * Build object MasterTable.
     *
     * @param string $code
     *            Code.
     * @param string $title
     *            Title.
     * @param string $description
     *            Description.
     */
    public function build($code, $title, $description = NULL)
    {
        $this->builder->create($code, $title, $description);
        return $this->builder->get();
    }
}