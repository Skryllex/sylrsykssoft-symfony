<?php
/**
 * @file
 * Builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\TranslatableBuilderTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
abstract class AbstractTranslatableBuilder implements ContainerAwareInterface
{
    use TranslatableBuilderTrait;
}