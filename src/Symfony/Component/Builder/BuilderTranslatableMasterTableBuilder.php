<?php
/**
 * @file
 * Translatable builder.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\TranslatableBuilderTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class BuilderTranslatableMasterTableBuilder implements ContainerAwareInterface
{
    use TranslatableBuilderTrait;

    /**
     * Build object MasterTable.
     *
     * @param string $code
     *            Code.
     * @param string $title
     *            Title.
     * @param string $description
     *            Description.
     */
    public function build($code, $title, $description = NULL)
    {
        $this->builder->create($code, $title, $description);
        return $this->builder->get();
    }
}