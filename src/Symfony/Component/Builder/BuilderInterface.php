<?php
/**
 * @file
 * Definition of the interface with methods to create objects.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

/**
 * Definition of the interface with methods to create objects.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
interface BuilderInterface
{
    /**
     * Return object.
     */
    public function get();
}