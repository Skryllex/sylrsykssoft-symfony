<?php
/**
 * @file
 * Implementation of MasterTableBuilderInterface.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

/**
 * Implementation of MasterTableBuilderInterface.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait MasterTableBuilderTrait
{

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderInterface::create()
     */
    public function create($code, $title, $description = NULL)
    {
        $this->object->setCode($code)
            ->setTitle($title)
            ->setDescription($description);
    }
}