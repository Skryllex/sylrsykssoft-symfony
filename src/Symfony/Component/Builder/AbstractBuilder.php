<?php
/**
 * @file
 * Builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\BuilderTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

abstract class AbstractBuilder implements ContainerAwareInterface
{
    use BuilderTrait;
}