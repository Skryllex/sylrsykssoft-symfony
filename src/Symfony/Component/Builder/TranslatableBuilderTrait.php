<?php
/**
 * @file
 * Translatable builder.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderTrait;
use SylrSyksSoftSymfony\Symfony\Component\Enum\SylrSyksSoftServices;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;

/**
 * Translatable builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait TranslatableBuilderTrait
{
    use BuilderTrait;

    /**
     * Translate object.
     *
     * @param ModelInterface $object
     *            Object.
     * @param array $translation
     *            Translation.
     * @return \SylrSyksSoftSymfony\CoreBundle\Model\AbstractModel
     *      Return object translated.
     */
    public function setTranslation(ModelInterface $object, array $translation)
    {
        if (! empty($translation)) {
            $translator = $this->container->get(SylrSyksSoftServices::TranslatorAdapter);
            $object_translated = $translator->translate($object, $translation);
            return $object_translated;
        }
        return $object;
    }
}