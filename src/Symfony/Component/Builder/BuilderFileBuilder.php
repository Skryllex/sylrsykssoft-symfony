<?php

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class BuilderFileBuilder implements ContainerAwareInterface
{
    use BuilderTrait;

    /**
     * Build object MasterTable.
     *
     * @param string $code
     *            Code.
     * @param string $title
     *            Title.
     * @param string $description
     *            Description.
     */
    public function build(UploadedFile $file, $filename, $mimetype, $length, $chunkSize, $md5)
    {
        $this->builder->create($file, $filename, $mimetype, $length, $chunkSize, $md5);
        return $this->builder->get();
    }
}