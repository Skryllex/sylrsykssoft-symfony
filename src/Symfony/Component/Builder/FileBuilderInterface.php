<?php
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileBuilderInterface extends BuilderInterface
{

    /**
     * Create object file.
     *
     * @param UploadedFile $file
     *            File
     * @param string $filename
     *            Filename.
     * @param string $mimetype
     *            Mimetype.
     * @param string $length
     *            Length
     * @param string $chunkSize
     *            Chunksize
     * @param string $md5
     *            MD5
     */
    public function create(UploadedFile $file, $filename, $mimetype, $length, $chunkSize, $md5);
}