<?php
/**
 * @file
 * Master Table Builder definition.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;

/**
 * Master Table Builder definition.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
interface MasterTableBuilderInterface extends BuilderInterface
{

    /**
     * Create a master table object.
     *
     * @param string $code
     *            Code.
     * @param string $title
     *            Title.
     * @param string $description
     *            Description (optional).
     */
    public function create($code, $title, $description = NULL);
}