<?php
/**
 * @file
 * Builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Builder;

use Doctrine\Common\Persistence\ManagerRegistry;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
abstract class AbstractObjectBuilder implements BuilderInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     *
     * @var ManagerRegistry
     */
    protected $manager;

    /**
     *
     * @var ModelInterface
     */
    protected $object;

    /**
     * Default constructor.
     *
     * @param ManagerRegistry $manager
     *            Manager.
     * @param ModelInterface $object
     *            Object.
     */
    public function __construct(ManagerRegistry $manager, ModelInterface $object)
    {
        $this->manager = $manager;
        $this->object = $object;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Builder\BuilderInterface::get()
     */
    public function get()
    {
        return $this->object;
    }
}