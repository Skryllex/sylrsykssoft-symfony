<?php
/**
 * @file
 * Singleton.
 *
 */
namespace SylrSyksSoftSymfony\CoreBundle\Util;

trait Singleton
{

    /**
     *
     * @var array The reference to *Singleton* instance of this class
     */
    protected static $_instances = array();

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function getInstance()
    {
        $class = get_called_class();
        if (! isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class();
        }
        return self::$_instances[$class];
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the 'new' operator from outside of this class.
     */
    protected function __construct()
    {}

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {}

    /**
     * Private serialize method to prevent serializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __sleep()
    {}

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {}
}