<?php
namespace SylrSyksSoftSymfony\CoreBundle\Util;

final class DateUtils
{
    use SylrSyksSoftSymfony\CoreBundle\Util\Singleton;

    const DATE_FORMAT_SHOW = 'dd-MMMM-yyyy';

    /**
     * Validate date.
     *
     * @param string $date
     *            Date.
     * @param string $format
     *            Format for date.
     * @return boolean
     */
    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return ($d && $d->format($format) == $date);
    }

    /**
     * Get date time of the string given.
     *
     * @param string $data
     *            String date.
     */
    public function createDate($data, $format = 'Y-m-d', $time = array())
    {
        if (strpos($data, 'T') !== FALSE) {
            $format = \DateTime::ATOM;
        }

        $d = \DateTime::createFromFormat($format, $data);

        if (empty($time)) {
            $d->setTime(0, 0);
        } else {
            $d->seTime($time['h'], $time['m'], $time['s']);
        }

        return $d;
    }
}