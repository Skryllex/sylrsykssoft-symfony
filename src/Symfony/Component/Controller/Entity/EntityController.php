<?php
/**
 * @file
 * Controller.
 */
namespace SylrSyksSoftSymfony\CoreBundle\Controller\Entity;

use Hostnet\Component\Form\Simple\SimpleFormProvider;
use JMS\DiExtraBundle\Annotation as JMSDiExtra;
use SylrSyksSoftSymfony\CoreBundle\Controller\AbstractController;
use SylrSyksSoftSymfony\CoreBundle\Entity\EntityManager;
use SylrSyksSoftSymfony\CoreBundle\Entity\Repository\EntityRepository;

class EntityController extends AbstractController
{
    /**
     *
     * @var EntityManager
     */
    protected $manager;

    /**
     *
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Default constructor.
     *
     * @param DocumentManager $manager
     *            Manager.
     * @param DocumentRepository $repository
     *            Repository.
     *
     * @JMSDiExtra\InjectParams({
     *      "manager" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core.manager.default"),
     *      "repository" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core.repository.default")
     * })
     */
    public function __construct(SimpleFormProvider $formProvider, EntityManager $manager, EntityRepository $repository)
    {
        parent::__construct($formProvider);
        $this->manager = $manager;
        $this->repository = $repository;
    }
}
