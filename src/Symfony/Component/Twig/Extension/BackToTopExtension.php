<?php
/**
 * @file
 * Twig Extension.
 */
namespace SylrSyksSoftSymfony\CoreBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class BackToTopExtension extends \Twig_Extension
{

    /**
     *
     * @var \Twig_Template
     */
    protected $template;

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     * @var string
     */
    protected $blockBackToTop;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = NULL)
    {
        $this->container = $container;
        $this->blockBackToTop = ($container->hasParameter('sylr_syks_soft_symfony_core.back_to_top.block_name')) ? $container->getParameter("sylr_syks_soft_symfony_core.back_to_top.block_name") : 'back_to_top';
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Twig_Extension::getFunctions()
     */
    public function getFunctions()
    {
        $options = array(
            'is_safe' => array(
                'html'
            ),
            'needs_environment' => TRUE
        );

        $functions = array(
            new \Twig_SimpleFunction($this->blockBackToTop, array(
                $this,
                'renderBackToTop'
            ), $options)
        );

        return $functions;
    }

    /**
     * Renders block back to top.
     *
     * @param string $icon
     *            Name icon.
     * @param string $translation
     *            String translation.
     * @param string $translation_domain
     *            Bundle name.
     * @param array $options
     *            An array of options.
     *
     * @return Response
     */
    public function renderBackToTop(\Twig_Environment $env, $icon, $translation, $translation_domain, array $options = array())
    {
        $template = $this->getTemplate($env);
        $context = array(
            'icon' => $icon,
            'translation' => $translation,
            'translation_domain' => $translation_domain,
            'options' => $options
        );

        return $template->renderBlock($this->blockBackToTop, $context);
    }

    /**
     * Get template.
     *
     * @return \Twig_Template
     */
    protected function getTemplate(\Twig_Environment $env)
    {
        if ($this->template === NULL) {
            $this->template = $env->loadTemplate('@SylrSyksSoftSymfonyCore/back_to_top.html.twig');
        }

        return $this->template;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Twig_ExtensionInterface::getName()
     */
    public function getName()
    {
        return 'back_to_top';
    }
}