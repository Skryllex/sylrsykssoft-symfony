<?php
/**
 * @file
 * Implementation BundleInterface.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Bundle;

/**
 * Implementation BundleInterface.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Bundle
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
trait BundleTrait
{

    /**
     * Get string bundle (className) of class.
     * String bundle contains AppBundle:className.
     *
     * @param string $bundleName
     *            Bundle name.
     */
    public static function getBundle($bundleName = 'AppBundle')
    {
        $class = new \ReflectionClass(static::class);
        $bundle = $bundleName . ':' . $class->getShortName();
        return $bundle;
    }
}