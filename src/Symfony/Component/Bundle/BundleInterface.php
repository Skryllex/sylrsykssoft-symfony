<?php
/**
 * @file
 * Bundle
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Bundle;

/**
 * Bundle.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Bundle
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
interface BundleInterface
{

    /**
     * Get string bundle (className) of class.
     * String bundle contains AppBundle:className.
     */
    public static function getBundle($bundleName = 'AppBundle');
}