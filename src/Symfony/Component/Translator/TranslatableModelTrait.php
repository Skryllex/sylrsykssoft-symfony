<?php
/**
 * @file
 * Defining the base model.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Translator;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Defining the base model.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Translator
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait TranslatableModelTrait
{
    /**
     *
     * @var string
     *
     * @Gedmo\Locale()
     */
    protected $locale;

    /**
     * Set locale.
     *
     * @param string $locale
     * @return \SylrSyksSoftSymfony\Symfony\Component\Translator\TranslatableModelTrait
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }
}