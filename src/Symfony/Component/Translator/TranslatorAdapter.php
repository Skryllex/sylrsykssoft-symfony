<?php
/**
 * @file
 * Translator Adapter.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Translator;

use Doctrine\Common\Persistence\ManagerRegistry;
use Gedmo\Tool\Wrapper\AbstractWrapper;
use Gedmo\Translatable\TranslatableListener;
use SylrSyksSoftSymfony\Symfony\Component\Enum\Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;

/**
 * Translator Adapter.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Translator
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
final class TranslatorAdapter
{

    /**
     *
     * @var ManagerRegistry
     */
    private $manager;

    /**
     *
     * @var TranslatableListener
     */
    private $listener;

    /**
     *
     * @var Translatable
     */
    private $repository;

    /**
     * Default constructor.
     *
     * @param ManagerRegistry $manager            
     * @param TranslatableListener $translatableListener            
     * @param string $repository
     *            Translator namespace possible values:
     *            - Gedmo::EntityRepositoryTranslatableNamespace
     *            - Gedmo::DocumentRepositoryTranslatableNamespace
     */
    public function __construct(ManagerRegistry $manager, TranslatableListener $translatableListener, $repository = Gedmo::EntityRepositoryTranslatableNamespace)
    {
        $this->manager = $manager;
        $this->listener = $translatableListener;
        $this->repository = $manager->getRepository($repository);
    }

    /**
     * Translate properties.
     *
     * @param array $translation
     *            Translation.
     * @param array $properties
     *            Properties to translation.
     */
    public function translate(AbstractModel $object, array $translation)
    {
        $manager = $this->manager->getManager();
        $translationConfig = $this->listener->getConfiguration($manager, get_class($object));
        if ($translationConfig && isset($translationConfig['fields'])) {
            foreach ($translationConfig['fields'] as $field) {
                $wrapped = AbstractWrapper::wrap($object, $manager);
                $propertyValue = $wrapped->getPropertyValue($field);
                if (is_array($propertyValue)) {
                    $wrapped->setPropertyValue($field, $propertyValue[$translationConfig['fallback'][$field]]);
                    foreach ($propertyValue as $local => $content) {
                        $this->repository->translate($object, $field, $local, $content);
                    }
                }
            }
        }
        
        return $object;
    }
}