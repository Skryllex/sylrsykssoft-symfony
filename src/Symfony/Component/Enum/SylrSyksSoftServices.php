<?php
/**
 * @file
 * Services.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum SylrSyksSoftServices.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Enum
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class SylrSyksSoftServices extends Enum
{

    const __default = self::BaseBuilder;

    const BaseBuilder = 'sylr_syks_soft_symfony_core.builder.object';

    const MasterTableBuilder = 'sylr_syks_soft_symfony_core.builder.master_table';

    const BuilderMasterTableBuilder = 'sylr_syks_soft_symfony_core.builder.master_table.builder';

    const BaseManager = 'sylr_syks_soft_symfony_core.manager';

    const ManagerMasterTable = 'sylr_syks_soft_symfony_core.manager.master_table';

    const DefaultController = 'sylr_syks_soft_symfony_core.controller';

    const DefaultRepository = 'sylr_syks_soft_symfony_core.repository';

    const TranslatorAdapter = 'sylr_syks_soft_symfony_core.translator.adapter';
}