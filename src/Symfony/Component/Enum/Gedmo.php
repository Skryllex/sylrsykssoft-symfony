<?php
/**
 * @file
 * Gedmo Translator Enum.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum Gedmo.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Enum
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class Gedmo extends Enum
{

    const ListenerTranslatable = 'gedmo.listener.translatable';

    const ListenerLoggable = 'gedmo.listener.loggable';

    const FilterSoftDeleteable = 'soft_deleteable';

    const FilterSoftDeleteableNamespace = 'Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter';

    const DocumentRepositoryTranslatableNamespace = 'Gedmo\\Translatable\\Document\\Translation';

    const EntityRepositoryTranslatableNamespace = 'Gedmo\\Translatable\\Entity\\Translation';

    const DocumentRepositoryLoggableNamespace = 'Gedmo\\Loggable\\Document\\LogEntry';

    const EntityRepositoryLoggableNamespace = 'Gedmo\Loggable\Entity\LogEntry';
}
