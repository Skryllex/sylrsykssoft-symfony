<?php
/**
 * @file
 *
 * User Enums.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Enum;

use MyCLabs\Enum\Enum;


/**
 * Enum Role.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Enum
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class Role extends Enum
{

    const __default = self::AuthenticatedUser;

    const AuthenticatedUser = "ROLE_AUTHENTICATED_USER";

    const AnonymousUser = "ROLE_ANONYMOUS_USER";

    const AllowedSwitch = 'ROLE_ALLOWED_TO_SWITCH';

    const Administrator = 'ROLE_USER';

    const SuperAdministrator = 'ROLE_SUPER_ADMIN';
}
