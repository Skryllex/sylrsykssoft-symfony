<?php
/**
 * @file
 * Service.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum Service.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Enum
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class Service extends Enum
{

    const __default = self::DoctrineIdentifier;

    const DoctrineIdentifier = 'doctrine';

    const DoctrineMongoDBIdentifier = 'doctrine_mongodb';

    const DoctrineMongoDBDocumentManager = 'doctrine.odm.mongodb.document_manager';
}