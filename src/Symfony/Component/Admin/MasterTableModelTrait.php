<?php
/**
 * @file
 * Implementation MasterTableModelInterface.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Admin;

/**
 * Implementation MasterTableModelInterface.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Admin\Common
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait MasterTableModelTrait
{

    /**
     *
     * @var string
     *
     */
    protected $code;

    /**
     *
     * @var string
     *
     */
    protected $title;

    /**
     *
     * @var string
     *
     */
    protected $description;

    /**
     *
     * @var string
     */
    protected $slug;

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::setCode()
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::getCode()
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::setTitle()
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::getTitle()
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::setDescription()
     */
    public function setDescription($description = NULL)
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::getDescription()
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\Symfony\Component\Admin\Common\MasterTableModelInterface::getSlug()
     */
    public function getSlug()
    {
        return $this->slug;
    }
}