<?php
/**
 * @file
 * Model default definition.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Admin;

use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;

/**
 * Model default definition.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model\Admin
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
interface MasterTableModelInterface extends ModelInterface
{

    /**
     * Set code.
     *
     * @param string $code
     */
    public function setCode($code);

    /**
     * Get code.
     */
    public function getCode();

    /**
     * Set title.
     *
     * @param string $title
     */
    public function setTitle($title);

    /**
     * Get title.
     */
    public function getTitle();

    /**
     * Set description.
     *
     * @param string $description
     */
    public function setDescription($description = NULL);

    /**
     * Get description.
     */
    public function getDescription();

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug();
}