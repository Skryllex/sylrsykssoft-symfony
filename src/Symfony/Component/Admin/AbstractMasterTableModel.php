<?php
/**
 * @file
 * Defining the base master table model.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Admin;

use SylrSyksSoftSymfony\Symfony\Component\Admin\MasterTableModelInterface;
use SylrSyksSoftSymfony\Symfony\Component\Admin\MasterTableModelTrait;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;

/**
 * Defining the base master table model.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model\Admin
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
abstract class AbstractMasterTableModel extends AbstractModel implements MasterTableModelInterface
{
    use MasterTableModelTrait;
}