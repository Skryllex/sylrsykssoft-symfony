<?php
/**
 * @file
 * Model default definition.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Model;

/**
 * Model default definition.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
interface ModelInterface
{

    /**
     * Get id.
     *
     * @return string
     */
    public function getId();

    /**
     * Set createAt.
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt = NULL);

    /**
     * Get createAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set updateAt.
     *
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt = NULL);

    /**
     * Get updateAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(\DateTime $deletedAt = NULL);

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt();
}