<?php
/**
 * @file
 * Base model trait.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Model;

/**
 * Base model trait.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait ModelTrait
{
    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::setCreatedAt()
     */
    public function setCreatedAt(\DateTime $createdAt = NULL)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::getCreatedAt()
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::setUpdatedAt()
     */
    public function setUpdatedAt(\DateTime $updatedAt = NULL)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::getUpdatedAt()
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::setDeletedAt()
     */
    public function setDeletedAt(\DateTime $deletedAt = NULL)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @see \SylrSyksSoft\Symfony\CoreBundle\Model\ModelInterface::getDeletedAt()
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}