<?php
/**
 * @file
 * Model file definition.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Model;

use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Model file definition.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
interface FileModelInterface extends ModelInterface
{
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file = NULL);

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile();

    /**
     * Set filename
     *
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename);

    /**
     * Get filename.
     *
     * @return string $filename.
     */
    public function getFilename();

    /**
     * Set mimeType.
     *
     * @param string $mimeType
     * @return $this
     */
    public function setMimeType($mimeType);

    /**
     * Get mimeType
     *
     * @return string $mimeType.
     */
    public function getMimeType();

    /**
     * Get chunkSize
     *
     * @return int $chunkSize.
     */
    public function getChunkSize();

    /**
     * Get length.
     *
     * @return int $length.
     */
    public function getLength();

    /**
     * Get md5.
     *
     * @return string $md5.
     */
    public function getMd5();

    /**
     * Get absolute path.
     *
     * @return NULL|string
     */
    public function getAbsolutePath();

    /**
     * Get web path.
     *
     *
     * @return NULL|string
     */
    public function getWebPath();

    /**
     * Get the absolute path of the directory where you should save files uploaded.
     *
     * @return string
     */
    public function getUploadRootDir();

    /**
     * Get relative upload dir.
     * Not include root dir.
     *
     * @return string
     */
    public function getUploadDir();
}