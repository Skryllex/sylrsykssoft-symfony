<?php
/**
 * @file
 * Defining the base model.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Model;

use SylrSyksSoftSymfony\Symfony\Component\Bundle\BundleInterface;
use SylrSyksSoftSymfony\Symfony\Component\Bundle\BundleTrait;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelTrait;

/**
 * Defining the base model.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Model\Common
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
abstract class AbstractModel implements BundleInterface, ModelInterface
{
    use BundleTrait;
    use ModelTrait;
}