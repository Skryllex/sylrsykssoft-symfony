<?php
namespace SylrSyksSoftSymfony\Symfony\Component\Model;

use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;
use SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Model file.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Model
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
abstract class AbstractFileModel extends AbstractModel implements FileModelInterface
{

    const UPLOAD_ROOT_DIR = __DIR__ . '/../../../../web/';

    const UPLOAD_DIR = "uploads/files";

    const INITIAL = 'initial';

    protected $temp;

    protected $file;

    protected $filename;

    protected $mimeType;

    protected $length;

    protected $chunkSize;

    protected $md5;

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::setFile()
     */
    public function setFile(UploadedFile $file = NULL)
    {
        $this->file = $file;
        // Check if we have an old image path.
        if (isset($this->path)) {
            // Store the old name to delete after the update.
            $this->temp = $this->path;
            $this->path = NULL;
        } else {
            $this->path = self::INITIAL;
        }

        $this->setFilename($file->getClientOriginalName());
        $this->setMimeType($file->getClientMimeType());

        return $this;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getFile()
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::setFilename()
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getFilename()
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::setMimeType()
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getMimeType()
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getChunkSize()
     */
    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getLength()
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getMd5()
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getAbsolutePath()
     */
    public function getAbsolutePath()
    {
        return (NULL === $this->path) ? NULL : $this->getUploadRootDir() . '/' . $this->path;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getWebPath()
     */
    public function getWebPath()
    {
        return (NULL === $this->path) ? NULL : $this->getUploadDir() . '/' . $this->path;
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getUploadDir()
     */
    public function getUploadDir()
    {
        return self::UPLOAD_DIR . '/' . __CLASS__ . '/files';
    }

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Model\FileModelInterface::getUploadRootDir()
     */
    public function getUploadRootDir()
    {
        return self::UPLOAD_ROOT_DIR . $this->getUploadDir();
    }
}