<?php
/**
 * @file
 * Handler utils.
 */

namespace SylrSyksSoftSymfony\Symfony\Component\Form\Handler;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Handler utils.
 * 
 * @packageSylrSyksSoftSymfony\CoreBundle\Form\Handler
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
trait HandlerTrait
{
    use ContainerAwareTrait;
    
    /**
     * Set message.
     * 
     * @param string $action
     * @param string $value
     */
    public function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}