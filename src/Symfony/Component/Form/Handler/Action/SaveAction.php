<?php
/**
 * @file
 * Save action handler.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action;

use SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action\AbstractAction;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Save action handler.
 *
 * @package SylrSyksSoftSymfony\CoreBundle\Form\Handler\Document
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
final class SaveAction extends AbstractAction
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormSuccessHandlerInterface::onSuccess()
     */
    public function onSuccess(Request $request)
    {
        $flush = TRUE;
        $object = $this->getData();
        
        $susccess = $this->manager->save($object, $flush);
        
        if ($susccess) {
            $this->setFlash('success', 'Element saved.');
        }
        
        return new RedirectResponse($this->router->generate($this->onSuccessPath));
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormFailureHandlerInterface::onFailure()
     */
    public function onFailure(Request $request)
    {
        $this->setFlash('danger', 'There were errors');
        
        if (! empty($this->onFailurePath)) {
            return new RedirectResponse($this->router->generate($this->onFailurePath));
        }
    }
}