<?php
/**
 * @file
 * Delete action handler.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action;

use SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action\AbstractAction;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Delete action handler.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
final class DeleteAction extends AbstractAction
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormSuccessHandlerInterface::onSuccess()
     */
    public function onSuccess(Request $request)
    {
        $flush = TRUE;
        $object = $this->getData();
        
        $susccess = $this->manager->delete($object, $flush);
        
        if ($susccess) {
            $this->setFlash('success', 'Element deleted.');
        }
        
        return new RedirectResponse($this->router->generate($this->onSuccessPath));
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormFailureHandlerInterface::onFailure()
     */
    public function onFailure(Request $request)
    {
        $this->setFlash('danger', 'There were errors');
        
        if (! empty($this->onFailurePath)) {
            return new RedirectResponse($this->router->generate($this->onFailurePath));
        }
    }
}