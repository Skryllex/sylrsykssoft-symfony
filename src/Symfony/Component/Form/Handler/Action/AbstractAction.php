<?php
/**
 * @file
 * Action handler.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action;

use Hostnet\Component\Form\AbstractFormHandler;
use Hostnet\Component\Form\FormFailureHandlerInterface;
use Hostnet\Component\Form\FormSuccessHandlerInterface;
use Sonata\CoreBundle\Model\ManagerInterface;
use SylrSyksSoftSymfony\CoreBundle\Form\Handler\HandlerTrait;
use SylrSyksSoftSymfony\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Action handler.
 *
 * @package SylrSyksSoftSymfony\Symfony\Component\Form\Handler\Action
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
abstract class AbstractAction extends AbstractFormHandler implements FormSuccessHandlerInterface, FormFailureHandlerInterface, ContainerAwareInterface
{
    use HandlerTrait;

    /**
     *
     * @var ManagerInterface
     */
    protected $manager;

    /**
     *
     * @var RouterInterface
     */
    protected $router;

    /**
     *
     * @var string
     */
    protected $onSuccessPath;

    /**
     *
     * @var string
     */
    protected $onFailurePath;

    /**
     * Default constructor.
     *
     * @param ManagerInterface $manager
     *            Manager.
     * @param RouterInterface $router
     *            Router.
     * @param string $onSuccessPath
     *            Succes path.
     * @param string $onFailurePath
     *            Failure path.
     */
    public function __construct(ManagerInterface $manager, RouterInterface $router, $onSuccessPath, $onFailurePath = '')
    {
        $this->manager = $manager;
        $this->router = $router;
        $this->onSuccessPath = $onSuccessPath;
        $this->onFailurePath = $onFailurePath;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormHandlerInterface::getType()
     */
    public function getType()
    {
        return AbstractType::class;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Hostnet\Component\Form\FormHandlerInterface::getData()
     */
    public function getData()
    {
        return $this->getForm()->getData();
    }
}