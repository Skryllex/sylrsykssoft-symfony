<?php
/**
 * @file
 * Form type.
 */
namespace SylrSyksSoftSymfony\Symfony\Component\Form\Type;

use Symfony\Component\Form\AbstractType as SymfonyAbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form type.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Component\Form\Type
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
abstract class AbstractType extends SymfonyAbstractType
{

    /**
     *
     * @var string
     */
    protected $class;

    /**
     *
     * @var string
     */
    protected $intention;

    /**
     *
     * @var array
     */
    protected $mergeOptions;

    /**
     *
     * @param string $class
     *            The class name
     * @param string $intention
     *            Action of form.
     * @param array $mergeOptions
     *            Add options to elements
     */
    public function __construct($class, $intention)
    {
        $this->class = $class;
        $this->intention = $intention;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Form\AbstractType::configureOptions()
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_protection' => TRUE,
            'csrf_field_name' => '_token',
            'intention' => $this->intention,
            'cascade_validation' => TRUE,
        ));
    }
}