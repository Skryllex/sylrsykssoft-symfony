Getting Started With SylrSyksSoftSymfonyCoreBundle
==================================================

The ``SylrSyksSoftSymfonyCoreNRDSBundle`` provides a number of common features for other projects.

Prerequisites
-------------

This version of the bundle requires Symfony 2.8.

Installation
------------

Installation is a quick (I promise!) 3 step process:

1. Download SylrSyksSoftSymfonyCoreNRDSBundle using composer
2. Enable the Bundle
3. Configure the SylrSyksSoftSymfonyCoreBundle

Step 1: Download SylrSyksSoftSymfonyCoreBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require sylrsykssoft-symfony/core-bundle/core-nrds-bundle "^1.0@dev"

Composer will install the bundle to your project's ``vendor/sylrsykssoft-symfony/core-bundle/core-nrds-bundle`` directory.

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new SylrSyksSoftSymfony\CoreBundle\SylrSyksSoftSymfonyCoreBundle(),
            new SylrSyksSoftSymfony\CoreBundle\NRDS\SylrSyksSoftSymfonyCoreNRDSBundle(),
            // ...
        );
    }

Next Steps
~~~~~~~~~~

Now that you have completed the basic installation and configuration of the
SylrSyksSoftSymfonyCoreBundle, you are ready to learn about more advanced features and usages
of the bundle.
