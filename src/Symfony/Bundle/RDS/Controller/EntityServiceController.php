<?php
/**
 * @file
 * Entity service controller.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Controller;

use Hostnet\Component\Form\Simple\SimpleFormProvider;
use JMS\DiExtraBundle\Annotation as JMSDiExtra;
use SylrSyksSoftSymfony\CoreBundle\Controller\AbstractServiceController;
use SylrSyksSoftSymfony\CoreBundle\Enum\Service;
use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\EntityManager;
use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\Repository\EntityRepository;

/**
 * Entity service controller.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Bundle\RDS\Controller
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
class EntityServiceController extends AbstractServiceController
{

    /**
     *
     * @var EntityManager
     */
    protected $manager;

    /**
     *
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * Default constructor.
     *
     * @param EntityManager $manager
     *            Manager.
     * @param EntityRepository $repository
     *            Repository.
     *            
     * @JMSDiExtra\InjectParams({
     *      "manager" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_rds.manager.default"),
     *      "repository" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_rds.repository.default")
     * })
     */
    public function __construct(SimpleFormProvider $formProvider, $bundle_name, $class_name, EntityManager $manager, EntityRepository $repository)
    {
        parent::__construct($formProvider, $bundle_name, $class_name);
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Bundle\FrameworkBundle\Controller\Controller::getDoctrine()
     */
    public function getDoctrine()
    {
        if (! $this->container->has(Service::DoctrineIdentifier)) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }
        
        return $this->container->get(Service::DoctrineIdentifier);
    }
}