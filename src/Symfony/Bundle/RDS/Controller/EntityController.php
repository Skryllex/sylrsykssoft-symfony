<?php
/**
 * @file
 * Controller.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Controller;

use Hostnet\Component\Form\Simple\SimpleFormProvider;
use JMS\DiExtraBundle\Annotation as JMSDiExtra;
use SylrSyksSoftSymfony\CoreBundle\Controller\AbstractController;
use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\EntityManager;
use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\Repository\EntityRepository;

class EntityController extends AbstractController
{
    /**
     *
     * @var EntityManager
     */
    protected $manager;

    /**
     *
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Default constructor.
     *
     * @param EntityManager $manager
     *            Manager.
     * @param EntityRepository $repository
     *            Repository.
     *
     * @JMSDiExtra\InjectParams({
     *      "manager" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_rds.manager.default"),
     *      "repository" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_rds.repository.default")
     * })
     */
    public function __construct(SimpleFormProvider $formProvider, EntityManager $manager, EntityRepository $repository)
    {
        parent::__construct($formProvider);
        $this->manager = $manager;
        $this->repository = $repository;
    }
}
