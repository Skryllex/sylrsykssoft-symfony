<?php

/*
 * This file is part of the SylrSyksSoft package.
 *
 * (c) Thomas Rabaix <sylar.sykes@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SylrSyksSoftSymfonyCoreRDSBundle extends Bundle {

}
