<?php
namespace SylrSyksSoftSymfony\CoreBundle\Tests\Builder;

use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Builder\EntityBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderTrait;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;

final class Seller extends AbstractModel
{

    private $nickname;

    private $firstName;

    private $lastName;

    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName = NULL)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getClass()
    {
        return static::class;
    }
}

interface SellerBuilderInterface extends BuilderInterface
{

    public function create($nickname, $firstName, $lastName = NULL);
}

final class SellerBuilder extends EntityBuilder implements SellerBuilderInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Tests\Builder\SellerBuilderInterface::create()
     */
    public function create($nickname, $firstName, $lastName = NULL)
    {
        $this->object->setNickname($nickname)
            ->setFirstName($firstName)
            ->setLastName($lastName);
    }
}

final class BuilderSeller
{
    use BuilderTrait;

    public function build($nickname, $firstName, $lastName = NULL)
    {
        $this->builder->create($nickname, $firstName, $lastName);
        return $this->builder->get();
    }
}

final class MockSeller
{

    public function get()
    {
        return array(
            'className' => Seller::class,
            'nickname' => 'john.doe',
            'first_name' => 'John',
            'last_name' => 'Doe'
        );
    }
}

final class EntityBuilderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Definition test.
     */
    public function test()
    {
        $customer = new Seller();

        $this->assertSame('SylrSyksSoftSymfony\CoreBundle\Tests\Builder\Seller', $customer->getClass());
    }

    /**
     * Test build.
     */
    public function testBuild()
    {
        $mock_customer = new MockSeller();
        $customer = $mock_customer->get();
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $customer_builder = new SellerBuilder($registry, $customer['className']);

        $builder = new BuilderSeller($customer_builder);

        $builder->build($customer['nickname'], $customer['first_name'], $customer['last_name']);
    }
}