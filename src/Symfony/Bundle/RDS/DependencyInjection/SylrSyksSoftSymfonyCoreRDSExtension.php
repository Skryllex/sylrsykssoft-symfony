<?php
/**
 * @file
 * Extension.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\DependencyInjection;

use SylrSyksSoftSymfony\CoreBundle\Enum\SylrSyksSoftServices;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SylrSyksSoftSymfonyCoreRDSExtension extends Extension
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\DependencyInjection\Extension\ExtensionInterface::load()
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load(sprintf('services/model-%s.yml', $config['db_driver']));
        $loader->load(sprintf('services/builder-%s.yml', $config['db_driver']));
        $loader->load(sprintf('services/form-%s.yml', $config['db_driver']));
        $loader->load(sprintf('services/controller-%s.yml', $config['db_driver']));
        $loader->load(sprintf('services/repository-%s.yml', $config['db_driver']));
        $loader->load(sprintf('services/translator-%s.yml', $config['db_driver']));

        // builders.
        $container->setAlias(SylrSyksSoftServices::BaseBuilder, $config['service']['base_builder']);
        $container->setAlias(SylrSyksSoftServices::MasterTableBuilder, $config['service']['builder_master_table']);
        $container->setAlias(SylrSyksSoftServices::BuilderMasterTableBuilder, $config['service']['builder_master_table_builder']);

        // managers.
        $container->setAlias(SylrSyksSoftServices::BaseManager, $config['service']['base_manager']);
        $container->setAlias(SylrSyksSoftServices::ManagerMasterTable, $config['service']['manager_master_table']);

        // controller.
        $container->setAlias(SylrSyksSoftServices::DefaultRepository, $config['service']['default_repository']);

        // translator.
        $container->setAlias(SylrSyksSoftServices::TranslatorAdapter, $config['service']['translator_adapter']);
    }
}
