<?php
/**
 * @file
 * Master Table builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\AbstractObjectBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderInterface;
use SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderTrait;

final class MasterTableEntityBuilder extends AbstractObjectBuilder implements MasterTableBuilderInterface
{
    use MasterTableBuilderTrait;
}