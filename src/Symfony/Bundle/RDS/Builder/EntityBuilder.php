<?php
/**
 * @file
 * Entity builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\AbstractObjectBuilder;

/**
 * Entity builder.
 *
 * @package SylrSyksSoftSymfony\Symfony\Bundle\RDS\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
class EntityBuilder extends AbstractObjectBuilder
{
}
