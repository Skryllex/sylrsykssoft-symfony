<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\AbstractEntity;
use SylrSyksSoftSymfony\Symfony\Component\Translator\TranslatableModelTrait;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractTranslatableEntity extends AbstractEntity implements Translatable
{
    use TranslatableModelTrait;
}