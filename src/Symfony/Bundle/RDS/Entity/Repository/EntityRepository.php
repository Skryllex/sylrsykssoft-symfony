<?php
/**
 * @file
 * Document repository.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\RDS\Entity\Repository;

use Doctrine\ORM\EntityRepository as BaseEntityRepository;
use SylrSyksSoftSymfony\Symfony\Component\Enum\Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;

class EntityRepository extends BaseEntityRepository
{

    /**
     * Finds a single document by the criteria slug.
     *
     * @param string $slug
     *            Slug value.
     * @return object
     */
    public function findBySlug($slug)
    {
        $criteria = array(
            'slug' => $slug
        );
        return $this->findOneBy($criteria);
    }

    /**
     * Find a single document by property.
     *
     * @param string $field
     *            Property.
     * @param mixed $value
     *            Value.
     * @return object|NULL
     */
    public function findOneByProperty($field, $value)
    {
        $query = $this->createQueryBuilder();
        $query->select('data');
        $query->andWhere($query->expr()
            ->eq($field, $value));
        return $query->getQuery()->getSingleResult();
    }

    /**
     * Get translations.
     *
     * @param ModelInterface $object
     */
    public function findTranslations(ModelInterface $object)
    {
        $repository = $this->_em->getRepository(Gedmo::EntityRepositoryTranslatableNamespace);
        return $repository->findTranslations($object);
    }
}