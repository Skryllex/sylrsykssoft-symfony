<?php
/**
 * @file
 * Document service controller.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Controller;

use Hostnet\Component\Form\Simple\SimpleFormProvider;
use JMS\DiExtraBundle\Annotation as JMSDiExtra;
use SylrSyksSoftSymfony\CoreBundle\Controller\AbstractServiceController;
use SylrSyksSoftSymfony\CoreBundle\Enum\Service;
use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\DocumentManager;
use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\Repository\DocumentRepository;

/**
 * Document service controller.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Controller
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *        
 */
class DocumentServiceController extends AbstractServiceController
{

    /**
     *
     * @var DocumentManager
     */
    protected $manager;

    /**
     *
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * Default constructor.
     *
     * @param DocumentManager $manager
     *            Manager.
     * @param DocumentRepository $repository
     *            Repository.
     *            
     * @JMSDiExtra\InjectParams({
     *      "manager" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_nrds.manager.default"),
     *      "repository" = @JMSDiExtra\Inject("sylr_syks_soft_symfony_core_nrds.repository.default")
     * })
     */
    public function __construct(SimpleFormProvider $formProvider, $bundle_name, $class_name, DocumentManager $manager, DocumentRepository $repository)
    {
        parent::__construct($formProvider, $bundle_name, $class_name);
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Bundle\FrameworkBundle\Controller\Controller::getDoctrine()
     */
    public function getDoctrine()
    {
        if (! $this->container->has(Service::DoctrineMongoDBIdentifier)) {
            throw new \LogicException('The DoctrineMongoDBBundle is not registered in your application.');
        }
        
        return $this->container->get(Service::DoctrineMongoDBIdentifier);
    }
}