<?php
/**
 * @file
 * Master table manager.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Sonata\DoctrineMongoDBAdminBundle\Model\ModelManager;

/**
 * Master table manager.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class MasterTableDocumentManager extends ModelManager
{

}