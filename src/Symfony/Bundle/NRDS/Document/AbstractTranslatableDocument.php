<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\AbstractDocument;
use SylrSyksSoftSymfony\Symfony\Component\Translator\TranslatableModelTrait;


/**
 * @MongoDB\MappedSuperclass()
 */
abstract class AbstractTranslatableDocument extends AbstractDocument implements Translatable
{
    use TranslatableModelTrait;
}