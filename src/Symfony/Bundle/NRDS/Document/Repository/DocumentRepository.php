<?php
/**
 * @file
 * Document repository.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository as BaseDocumentRepository;
use SylrSyksSoftSymfony\Symfony\Component\Enum\Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Model\ModelInterface;

class DocumentRepository extends BaseDocumentRepository
{

    /**
     * Finds a single document by the criteria slug.
     *
     * @param string $slug
     *            Slug value.
     * @return object
     */
    public function findBySlug($slug)
    {
        $criteria = array(
            'slug' => $slug
        );
        return $this->findOneBy($criteria);
    }

    /**
     * Find a single document by property.
     *
     * @param string $field
     *            Property.
     * @param mixed $value
     *            Value.
     * @return object|NULL
     */
    public function findOneByProperty($field, $value)
    {
        return $this->createQueryBuilder()
            ->field($field)
            ->equals($value)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Get translations.
     *
     * @param ModelInterface $object
     */
    public function findTranslations(ModelInterface $object)
    {
        $repository = $this->dm->getRepository(Gedmo::DocumentRepositoryTranslatableNamespace);
        return $repository->findTranslations($object);
    }
}