<?php
/**
 * @file
 * Master table document.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Admin\AbstractMasterTableModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Unique(fields={"code", "title"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable()
 */
abstract class AbstractMasterTableDocument extends AbstractMasterTableModel
{

    /**
     *
     * @var string
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      min=3,
     *      max=50,
     *      minMessage="The code is too short.",
     *      maxMessage="The code is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $code;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $description;

    /**
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\Length(
     *      max=128,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Slug(fields={"title"})
     */
    protected $slug;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="created_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="updated_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="deleted_at", nullable=true)
     * @Assert\DateTime()
     */
    protected $deletedAt;
}