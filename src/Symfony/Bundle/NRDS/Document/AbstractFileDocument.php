<?php
/**
 * @file
 * Document file.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractFileModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable()
 */
abstract class AbstractFileDocument extends AbstractFileModel
{
    /**
     *
     * @var string
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @MongoDB\String()
     */
    protected $filename;

    /**
     *
     * @var string
     *
     * @MongoDB\String()
     */
    protected $mimeType;

    /**
     *
     * @var integer
     *
     * @MongoDB\Int()
     */
    protected $length;

    /**
     *
     * @var integer
     *
     * @MongoDB\Int()
     */
    protected $chunkSize;

    /**
     *
     * @var string
     *
     * @MongoDB\String()
     */
    protected $md5;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="created_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="updated_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="deleted_at", nullable=true)
     * @Assert\DateTime()
     */
    protected $deletedAt;
}