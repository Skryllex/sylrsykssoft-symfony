<?php
/**
 * @file
 * Document manager.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Gedmo\Exception\UnexpectedValueException;
use Sonata\CoreBundle\Model\BaseDocumentManager;
use SylrSyksSoftSymfony\Symfony\Component\Enum\Gedmo;

/**
 * Document manager.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class DocumentManager extends BaseDocumentManager
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Sonata\CoreBundle\Model\BaseManager::save()
     */
    public function save($entity, $andFlush = TRUE)
    {
        $success = FALSE;
        $this->checkObject($entity);

        $entityUnChanged = $this->getUnchangedDocument($entity);

        try {
            $this->getObjectManager()->persist($entity);

            if ($andFlush) {
                $this->getObjectManager()->flush();
            }

            $success = TRUE;
        } catch (Exception $e) {
            $success = FALSE;
            if ($entityUnChanged !== FALSE) {
                try {
                    $this->getObjectManager()->persist($entityUnChanged);
                    $this->getObjectManager()->flush();
                } catch (Exception $e) {
                    $success = FALSE;
                }
            }
        }

        return $success;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Sonata\CoreBundle\Model\BaseManager::delete()
     */
    public function delete($entity, $andFlush = TRUE)
    {
        $success = FALSE;
        $this->checkObject($entity);

        $entityUnChanged = $this->getUnChangedDocument($entity);

        try {
            $this->getObjectManager()->remove($entity);

            if ($andFlush) {
                $this->getObjectManager()->flush();
            }

            $success = TRUE;
        } catch (Exception $e) {
            $success = FALSE;
            try {
                $this->getObjectManager()->persist($entityUnChanged);
                $this->getObjectManager()->flush();
            } catch (Exception $e) {
                $success = FALSE;
            }
        }

        return $success;
    }

    /**
     * Get unchanged document.
     *
     * @param object $entity
     *            Object.
     * @param int $version
     *            Number version.
     * @return object Return the object to the given version.
     */
    public function getUnchangedDocument($entity, $version = 1)
    {
        $repository = $this->getObjectManager()->getRepository(Gedmo::DocumentRepositoryLoggableNamespace);
        try {
            $repository->revert($entity, $version);
        } catch (UnexpectedValueException $e) {
            return FALSE;
        }

        return $entity;
    }
}