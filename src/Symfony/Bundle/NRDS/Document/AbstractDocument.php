<?php
/**
 * @file
 * Defining the basic document with common properties for documents.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class AbstractDocument extends AbstractModel
{

    /**
     *
     * @var string
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="created_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="updated_at")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(name="deleted_at", nullable=true)
     * @Assert\DateTime()
     */
    protected $deletedAt;
}