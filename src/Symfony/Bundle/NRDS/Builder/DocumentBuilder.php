<?php
/**
 * @file
 * Document builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\AbstractObjectBuilder;

/**
 * Document builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\NRDS\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
class DocumentBuilder extends AbstractObjectBuilder
{

}