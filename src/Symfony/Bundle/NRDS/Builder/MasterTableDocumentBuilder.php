<?php
/**
 * @file
 * Master Table builder.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\NRDS\Builder;

use SylrSyksSoftSymfony\Symfony\Component\Builder\AbstractObjectBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderInterface;
use SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderTrait;

/**
 * Master Table builder.
 * 
 * @package SylrSyksSoftSymfony\CoreBundle\NRDS\Builder
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
final class MasterTableDocumentBuilder extends AbstractObjectBuilder implements MasterTableBuilderInterface
{
    use MasterTableBuilderTrait;
}