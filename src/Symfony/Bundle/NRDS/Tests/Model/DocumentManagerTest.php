<?php
/**
 * @file
 * Model test.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Tests\Model;

use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\DocumentManager;

final class DocumentManagerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Get manager.
     *
     * @return \SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Document\DocumentManager
     */
    public function getManager()
    {
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');

        $manager = new DocumentManager('classname', $registry);

        return $manager;
    }

    /**
     * Definition test.
     */
    public function test()
    {
        $this->assertSame('classname', $this->getManager()->getClass());
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The property exception does not exists
     */
    public function testException()
    {
        $this->getManager()->exception;
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Unable to find the mapping information for the class classname.
     * Please check the 'auto_mapping' option (http://symfony.com/doc/current/reference/configuration/doctrine.html#configuration-overview) or add the bundle to the 'mappings' section in the doctrine configuration
     */
    public function testExceptionOnNonMappedDocument()
    {
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $registry->expects($this->once())
        ->method('getManagerForClass')
        ->will($this->returnValue(null));

        $manager = new DocumentManager('classname', $registry);
        $manager->__get('dm');
        $manager->getDocumentManager();
    }

    /**
     * Get document manager.
     */
    public function testGetDocumentManager()
    {
        $objectManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');

        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $registry->expects($this->once())
        ->method('getManagerForClass')
        ->will($this->returnValue($objectManager));

        $manager = new DocumentManager('classname', $registry);

        $manager->dm;
    }
}