SylrSyksSoftSymfonyCoreNRDSBundle Tests
=======================================

The ``SylrSyksSoftSymfonyCoreNRDSBundle`` provides a number of common features for other projects.

Prerequisites
-------------

To run the tests have set the following files:

- phpunit.xml.dist `Configure file <https://phpunit.de/manual/current/en/appendixes.configuration.html>`
- configure autoload in composer.json with::
    "autoload-dev" : {
      "psr-4" : {
         "SylrSyksSoftSymfony\\CoreBundle\\NRDS\\Tests\\" : "Tests/"
      }
   },
- configure bootstrap.php file ``NRDS/Tests/tests/bootstrap.php``
- configure autoload.php file ``NRDS/Tests/tests/bootstrap.php.dist``

Execute tests
-------------

Builder
~~~~~~~

- phpunit NRDS/Tests/Builder/DocumentBuilderTest.php
- phpunit NRDS/Tests/Builder/MasterTableBuilderTest.php

Model
~~~~~

- phpunit NRDS/Tests/Model/DocumentManagerTest.php
