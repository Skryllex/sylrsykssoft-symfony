<?php
/**
 * @file
 * Model test.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Tests\Builder;

use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Builder\DocumentBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderInterface;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderTrait;
use SylrSyksSoftSymfony\Symfony\Component\Model\AbstractModel;

final class Customer extends AbstractModel
{

    private $nickname;

    private $firstName;

    private $lastName;

    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName = NULL)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getClass() {
        return static::class;
    }
}

interface CustomerBuilderInterface extends BuilderInterface
{

    public function create($nickname, $firstName, $lastName = NULL);
}

final class CustomerBuilder extends DocumentBuilder implements CustomerBuilderInterface
{

    /**
     * 
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Tests\Builder\CustomerBuilderInterface::create()
     */
    public function create($nickname, $firstName, $lastName = NULL)
    {
        $this->object->setNickname($nickname)
            ->setFirstName($firstName)
            ->setLastName($lastName);
    }
}

final class BuilderCustomer
{
    use BuilderTrait;

    public function build($nickname, $firstName, $lastName = NULL) {
        $this->builder->create($nickname, $firstName, $lastName);
        return $this->builder->get();
    }
}

final class MockCustomer
{

    public function get()
    {
        return array(
            'className' => Customer::class,
            'nickname' => 'john.doe',
            'first_name' => 'John',
            'last_name' => 'Doe'
        );
    }
}

final class DocumentBuilderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Definition test.
     */
    public function test()
    {
        $customer = new Customer();

        $this->assertSame('SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Tests\Builder\Customer', $customer->getClass());
    }

    /**
     * Test build.
     */
    public function testBuild() {
        $mock_customer = new MockCustomer();
        $customer = $mock_customer->get();
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $customer_builder = new CustomerBuilder($registry, $customer['className']);

        $builder = new BuilderCustomer($customer_builder);

        $builder->build($customer['nickname'], $customer['first_name'], $customer['last_name']);
    }
}