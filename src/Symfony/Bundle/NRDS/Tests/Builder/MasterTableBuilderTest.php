<?php
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Tests\Builder;

use SylrSyksSoftSymfony\Symfony\Bundle\NRDS\Builder\DocumentBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Admin\AbstractMasterTableModel;
use SylrSyksSoftSymfony\Symfony\Component\Builder\BuilderMasterTableBuilder;
use SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderInterface;

final class TypeSeller extends AbstractMasterTableModel
{
    public function getClass() {
        return static::class;
    }
}

final class TypeSellerBuilder extends DocumentBuilder implements MasterTableBuilderInterface
{

    /**
     * {@inheritDoc}
     * 
     * @see \SylrSyksSoftSymfony\Symfony\Component\Builder\MasterTableBuilderInterface::create()
     */
    public function create($code, $title, $description = NULL)
    {
        $this->object->setCode($code)
            ->setTitle($title)
            ->setDescription($description);
    }
}

final class MockTypeSeller
{

    public function get()
    {
        return array(
            'className' => TypeSeller::class,
            'code' => 'TRHJU-GG',
            'title' => 'Wholesaler',
            'description' => 'They are the ones who are responsible for buying certain products in bulk and reselling them at a higher price to retailers, who should buy it wholesale for profit of intermediation.'
        );
    }
}

final class MasterTableBuilderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Definition test.
     */
    public function test()
    {
        $type = new TypeSeller();

        $this->assertSame('SylrSyksSoftSymfony\CoreBundle\Tests\Builder\TypeSeller', $type->getClass());
    }

    /**
     * Test build.
     */
    public function testBuild() {
        $mock_type = new MockTypeSeller();
        $type = $mock_type->get();
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $type_seller_builder = new TypeSellerBuilder($registry, $type['className']);

        $builder = new BuilderMasterTableBuilder($type_seller_builder);

        $builder->build($type['code'], $type['title'], $type['description']);
    }
}