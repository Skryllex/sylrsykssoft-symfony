<?php
/**
 * @file
 * Bundle.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle.
 * 
 * @package SylrSyksSoftSymfony\Symfony\Bundle\NRDS
 * @author Juan Gonzalez Fernandez <sylar.sykes@gmail.com>
 *
 */
class SylrSyksSoftSymfonyCoreNRDSBundle extends Bundle {

}
