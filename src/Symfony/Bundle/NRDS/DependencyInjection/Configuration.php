<?php
/**
 * @file
 * Configuration bundle.
 */
namespace SylrSyksSoftSymfony\Symfony\Bundle\NRDS\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @link http://symfony.com/doc/current/cookbook/bundles/configuration.html
 */
class Configuration implements ConfigurationInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Config\Definition\ConfigurationInterface::getConfigTreeBuilder()
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sylr_syks_soft_symfony_core_nrds');

        $supportedDrivers = array(
            'mongodb'
        );

        $rootNode
            ->children()
                ->scalarNode('db_driver')
                    ->validate()
                        ->ifNotInArray($supportedDrivers)
                        ->thenInvalid('The driver %s is not supported. Please choose one of ' . json_encode($supportedDrivers))
                    ->end()
                    ->cannotBeOverwritten()
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('service')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('base_builder')->defaultValue('sylr_syks_soft_symfony_core_nrds.builder.object.default')->end()
                        ->scalarNode('builder_master_table')->defaultValue('sylr_syks_soft_symfony_core_nrds.builder.master_table.default')->end()
                        ->scalarNode('builder_master_table_builder')->defaultValue('sylr_syks_soft_symfony_core_nrds.builder.master_table.builder.default')->end()
                        ->scalarNode('base_manager')->defaultValue('sylr_syks_soft_symfony_core_nrds.manager.default')->end()
                        ->scalarNode('manager_master_table')->defaultValue('sylr_syks_soft_symfony_core_nrds.manager.master_table.default')->end()
                        ->scalarNode('default_repository')->defaultValue('sylr_syks_soft_symfony_core_nrds.repository.default')->end()
                        ->scalarNode('translator_adapter')->defaultValue('sylr_syks_soft_symfony_core_nrds.translator.adapter.default')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
